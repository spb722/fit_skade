from sqlalchemy import Column, Integer, Float, String, ForeignKey, Boolean
from sqlalchemy.orm import relationship
from database import Base


class Food(Base):
    __tablename__ = "foods"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True)
    timing = Column(String, index=True)  # e.g., "breakfast", "lunch", etc.
    serving_sizes = relationship("ServingSize", back_populates="food")
    macronutrients = relationship("Macronutrient", back_populates="food")
    micronutrients = relationship("Micronutrient", back_populates="food")


class Macronutrient(Base):
    __tablename__ = "macronutrients"

    id = Column(Integer, primary_key=True, index=True)
    food_id = Column(Integer, ForeignKey("foods.id"))

    carbohydrates_g = Column(Float, nullable=True)
    proteins_g = Column(Float, nullable=True)
    fats_g = Column(Float, nullable=True)
    dietary_fiber_g = Column(Float, nullable=True)
    sugars_g = Column(Float, nullable=True)  # Subset of carbohydrates

    food = relationship("Food", back_populates="macronutrients")


class ServingSize(Base):
    __tablename__ = "serving_sizes"

    id = Column(Integer, primary_key=True, index=True)
    food_id = Column(Integer, ForeignKey("foods.id"))
    description = Column(String, index=True)  # e.g., "cup", "jumbo", "large", etc.
    weight_g = Column(Float)  # Weight of this serving size in grams

    food = relationship("Food", back_populates="serving_sizes")
    macronutrients = relationship("Macronutrient", back_populates="serving_size")
    micronutrients = relationship("Micronutrient", back_populates="serving_size")


class Micronutrient(Base):
    __tablename__ = "micronutrients"

    id = Column(Integer, primary_key=True, index=True)
    food_id = Column(Integer, ForeignKey("foods.id"))

    vitamin_a_mcg = Column(Float, nullable=True)
    vitamin_c_mg = Column(Float, nullable=True)
    vitamin_d_mcg = Column(Float, nullable=True)
    vitamin_e_mg = Column(Float, nullable=True)
    vitamin_k_mcg = Column(Float, nullable=True)
    thiamin_mg = Column(Float, nullable=True)
    riboflavin_mg = Column(Float, nullable=True)
    niacin_mg = Column(Float, nullable=True)
    vitamin_b6_mg = Column(Float, nullable=True)
    folate_mcg = Column(Float, nullable=True)
    vitamin_b12_mcg = Column(Float, nullable=True)
    pantothenic_acid_mg = Column(Float, nullable=True)
    biotin_mcg = Column(Float, nullable=True)
    choline_mg = Column(Float, nullable=True)
    calcium_mg = Column(Float, nullable=True)
    iron_mg = Column(Float, nullable=True)
    magnesium_mg = Column(Float, nullable=True)
    phosphorus_mg = Column(Float, nullable=True)
    potassium_mg = Column(Float, nullable=True)
    sodium_mg = Column(Float, nullable=True)
    zinc_mg = Column(Float, nullable=True)
    copper_mg = Column(Float, nullable=True)
    manganese_mg = Column(Float, nullable=True)
    selenium_mcg = Column(Float, nullable=True)
    chromium_mcg = Column(Float, nullable=True)
    molybdenum_mcg = Column(Float, nullable=True)
    iodine_mcg = Column(Float, nullable=True)

    food = relationship("Food", back_populates="micronutrients")
