# @Filename:    service.py
# @Author:      spb72
# @Time:        18-09-2023 17:10
# @Email:
from sqlalchemy.orm import Session
from models import Food, Macronutrient, Micronutrient

def create_food(session: Session, food_data):
    food = Food(**food_data)
    session.add(food)
    session.commit()
    session.refresh(food)
    return food

def get_food(session: Session, food_id: int):
    return session.query(Food).filter(Food.id == food_id).first()

# Similar functions for Macronutrient and Micronutrient...

