from fastapi import FastAPI, Depends
from sqlalchemy.orm import Session
from database import SessionLocal, engine
import models, service

models.Base.metadata.create_all(bind=engine)

app = FastAPI()

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

@app.post("/foods/")
def create_food(food_data: models.Food, db: Session = Depends(get_db)):
    return service.create_food(db, food_data)

@app.get("/foods/{food_id}")
def read_food(food_id: int, db: Session = Depends(get_db)):
    return service.get_food(db, food_id)

# Additional routes for other operations...
